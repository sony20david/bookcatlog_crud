import axios from 'axios';

const usersUrl = 'http://localhost:4000/users';

export const getBooks = async (id) => {
    id = id || '';
    return await axios.get(`${usersUrl}/${id}`);
}

export const addBook = async (book) => {
    return await axios.post(`${usersUrl}/add`, book);
}

export const deleteBook = async (id) => {
    return await axios.delete(`${usersUrl}/${id}`);
}

export const editBook = async (id, book) => {
    return await axios.put(`${usersUrl}/${id}`, book)
}