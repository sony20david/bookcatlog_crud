import AllBook from './Component/AllBook';
import AddBook from './Component/AddBook';
import EditBook from './Component/EditBook';
import NavBar from './Component/NavBar';
import Home from './Component/Home';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/all" component={AllBook} />
        <Route exact path="/add" component={AddBook} />
        <Route exact path="/edit/:id" component={EditBook} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
