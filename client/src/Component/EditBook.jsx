import { useState, useEffect } from 'react';
import { FormGroup, FormControl, InputLabel, Input, Button, makeStyles, Typography } from '@material-ui/core';
import { useHistory, useParams } from 'react-router-dom';
import { getBooks, editBook } from '../Service/api';

const initialValue = {
    bookname: '',
    book_description: '',
    book_price: '',
    book_discount: '',
    book_availability:'',
    book_author:'',
    book_publisher:''
}

const useStyles = makeStyles({
    container: {
        width: '50%',
        margin: '5% 0 0 25%',
        '& > *': {
            marginTop: 20
        }
    }
})

const EditBook = () => {
    const [book, setBook] = useState(initialValue);
    const { bookname, book_description, book_price, book_discount ,book_availability,book_author,book_publisher } = book;
    const { id } = useParams();
    const classes = useStyles();
    let history = useHistory();

    useEffect(() => {
        loadBookDetails();
    }, []);

    const loadBookDetails = async() => {
        const response = await getBooks(id);
        setBook(response.data);
    }

    const editBookDetails = async() => {
        const response = await editBook(id, book);
        history.push('/all');
    }

    return (
        <FormGroup className={classes.container}>
            <Typography variant="h4">Edit Bookdetails</Typography>
            <FormControl>
                <InputLabel htmlFor="my-input">bookname</InputLabel>
                <Input onChange={(e) => setBook({...book,bookname:e.target.value})} name='name' value={bookname} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_desciption</InputLabel>
                <Input onChange={(e) => setBook({...book,book_description:e.target.value})} name='username' value={book_description} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_price</InputLabel>
                <Input onChange={(e) => setBook({...book,book_price:e.target.value})} name='email' value={book_price} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_discount</InputLabel>
                <Input onChange={(e) => setBook({...book,book_discount:e.target.value})} name='phone' value={book_discount} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_availability</InputLabel>
                <Input onChange={(e) => setBook({...book,book_availability:e.target.value})} name='phone' value={book_availability} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_author</InputLabel>
                <Input onChange={(e) => setBook({...book,book_author:e.target.value})} name='phone' value={book_author} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_publisher</InputLabel>
                <Input onChange={(e) => setBook({...book,book_publisher:e.target.value})} name='phone' value={book_publisher} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <Button variant="contained" color="primary" onClick={() => editBookDetails()}>Edit Book</Button>
            </FormControl>
        </FormGroup>
    )
}

export default EditBook;