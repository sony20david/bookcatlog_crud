import react, { useState } from 'react';
import { FormGroup, FormControl, InputLabel, Input, Button, makeStyles, Typography } from '@material-ui/core';
import { addBook } from '../Service/api';
import { useHistory } from 'react-router-dom';

const initialValue = {
    bookname: '',
    book_description: '',
    book_price: '',
    book_discount: '',
    book_availability:'',
    book_author:'',
    book_publisher:''
}

const useStyles = makeStyles({
    container: {
        width: '50%',
        margin: '5% 0 0 25%',
        '& > *': {
            marginTop: 20
        }
    }
})

const AddBook = () => {
    const [book, setBook] = useState(initialValue);
    const { bookname, book_description, book_price, book_discount ,book_availability,book_author,book_publisher} = book;
    const classes = useStyles();
    let history = useHistory();

    const addBookDetails = async() => {
        await addBook(book);
        history.push('./all');
    }

    return (
        <FormGroup className={classes.container}>
            <Typography variant="h4">Add Book</Typography>
            <FormControl>
                <InputLabel htmlFor="my-input">Name</InputLabel>
                <Input onChange={(e) => setBook({...book,bookname:e.target.value})} name='bookname' value={book.bookname} id="my-input" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">Description</InputLabel>
                <Input onChange={(e) => setBook({...book,book_description:e.target.value})} name='description' value={book.book_description} id="my-input" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">price</InputLabel>
                <Input onChange={(e) => setBook({...book,book_price:e.target.value})} name='price' value={book.book_price} id="my-input"/>
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">discount</InputLabel>
                <Input onChange={(e) => setBook({...book,book_discount:e.target.value})} name='discount' value={book.book_discount} id="my-input" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">availability</InputLabel>
                <Input onChange={(e) => setBook({...book,book_availability:e.target.value})} name='availability' value={book.book_availability} id="my-input" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">author</InputLabel>
                <Input onChange={(e) => setBook({...book,book_author:e.target.value})} name='author' value={book.book_author} id="my-input" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">publisher</InputLabel>
                <Input onChange={(e) => setBook({...book,book_publisher:e.target.value})} name='publisher' value={book.book_publisher} id="my-input" />
            </FormControl>
            <FormControl>
                <Button variant="contained" color="primary" onClick={() => addBookDetails()}>Add Book</Button>
            </FormControl>
        </FormGroup>
    )
}

export default AddBook;