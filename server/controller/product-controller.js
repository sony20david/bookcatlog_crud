import { request } from 'express';
import postBook from '../model/product.js';


export const getBooks=async(request,response)=>{
    try {
        const books=await postBook.find();
        response.status(200).json(books)
    } catch (error) {
        response.status(404).json({message:error.message})
    }
}

export const addBook=async(request,response)=>{
    const book=request.body;
    console.log("inside")
    const newBook=new postBook(book);
    try {
        await newBook.save();
        console.log("dbsaved")
        response.status(201).json(newBook);
    } catch (error) {
        response.status(409).json({message:error.message})
    }
}

export const getBookById=async(request,response)=>{
    try {
        const book=await postBook.findById(request.params.id);
            response.status(209).json(book);
    } catch (error) {
        response.status(404).json({message:error.message})     
    }
}


export const editBook=async(request,response)=>{
    let book=await postBook.findById(request.params.id)
    book=request.body;
    const editBook=new postBook(book)
    try {
        await postBook.updateOne({_id:request.params.id},editBook);
        response.status(201).json(editBook)
    } catch (error) {
        response.status(409).json({message:error.message})       
    }
}

export const deleteBook=async(request,response)=>{
    try {
        await postBook.deleteOne({_id:request.params.id});
        response.status(201).json("user deleted")
    } catch (error) {
        response.status(409).json({message:error.message})
    }
}