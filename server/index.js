import express from "express";
import bodyparser from "body-parser";
import dotenv from 'dotenv';
import mongoose from "mongoose";
import cors from 'cors';

import Routes from './server/routes.js';

const app=express();


app.use(bodyparser.json({extended:true}));
app.use(bodyparser.urlencoded({extended:true}))
app.use(cors());



// import {getBooks,addBook,getBookById,editBook,deleteBook}from './controller/product-controller.js';

// app.get('/',getBooks);
// app.post('/add',addBook);
// app.get('/:id',getBookById);
// app.put('/:id',editBook);
// app.delete('/:id',deleteBook);
app.use('/users', Routes);



const URL="mongodb+srv://online_book:book@cluster0.bkfzy.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

const PORT=process.env.PORT||'4000';


// mongoose.connect(URL,{
//     useNewUrlParser:true,
//     useUnifiedTopology:true,
//     useFindAndModify:false
// }).then(()=>{
//     app.listen(PORT,()=>{
//         console.log(`server is running on PORT:${PORT} `)
//     }).catch((error)=>{
//         console.log('error',error.message)
//     })
// })
mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true}).then(() => { 
    // we need .then becausew
    //it returns a promise 
    app.listen(PORT, () => console.log(`Server is running on PORT: ${PORT}`))
}).catch((error) => {
    console.log('Error:', error.message)
})